import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, ManyToOne } from 'typeorm';
import { AbstractEntity } from '../shared/dtos/abstract.dto';
import { ClothEntity } from './cloth.entity';
import { OrderEntity } from './order.entity';
import { UserEntity } from './user.entity';

@Entity('clothes_in_order')
export class ClothesInOrder extends AbstractEntity {
  @Column()
  public orderId!: string;

  @ApiProperty()
  @Column()
  public clothId!: string;

  @ApiProperty()
  @Column({ nullable: true })
  quantity: number;

  @ApiProperty()
  @Column({ nullable: true })
  unitPrice: number;

  @Column({ nullable: true })
  totalPrice: number;

  @ManyToOne(() => OrderEntity, (order) => order.id, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  public order!: OrderEntity;

  @ManyToOne(() => ClothEntity, (cloth) => cloth.id, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  public cloth!: ClothEntity;
}
