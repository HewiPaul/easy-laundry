import { ApiProperty } from '@nestjs/swagger';
import { classToPlain } from 'class-transformer';
import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { AbstractEntity } from '../shared/dtos/abstract.dto';
import { ClothesInOrder } from './clothesinorder.entity';
import { UserEntity } from './user.entity';

@Entity('order')
export class OrderEntity extends AbstractEntity {
  @ApiProperty()
  @Column('text')
  title: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  type: string;

  @ApiProperty()
  @Column('timestamptz', { nullable: true })
  orderDate: Date;

  @ApiProperty()
  @Column('timestamptz', { nullable: true })
  dueDate: Date;

  @ManyToOne(() => UserEntity, { cascade: true, onDelete: 'CASCADE' })
  owner: UserEntity;

  @ApiProperty()
  @Column('text', { nullable: true })
  ownerId: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  status: string;

  @ApiProperty()
  @Column({ nullable: true })
  totalCost: number;

  @OneToMany(() => ClothesInOrder, (clothes) => clothes.order)
  @ApiProperty({ type: () => [ClothesInOrder] })
  public orderedClothes!: ClothesInOrder[];

  toJSON() {
    return classToPlain(this);
  }
}
