import { ApiProperty } from '@nestjs/swagger';
import { classToPlain, Exclude, Expose, plainToClass } from 'class-transformer';
import { BeforeInsert, Column, Entity, ManyToOne } from 'typeorm';
import { AbstractEntity } from '../shared/dtos/abstract.dto';
import { UserEntity } from './user.entity';

@Entity('organization')
export class OrganizationEntity extends AbstractEntity {
  @ApiProperty()
  @Column('varchar', { length: 500 })
  public name: string;

  @ApiProperty()
  @Column('varchar', { length: 500, nullable: true })
  public shortName: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  public type: string;

  @ApiProperty()
  @Column('text', { array: true })
  phones: string[];

  @ApiProperty({ type: () => JSON, nullable: true })
  @Column('jsonb', { nullable: true })
  location: string;

  @ApiProperty()
  @Column()
  public isActive: boolean;

  @ApiProperty()
  @Column()
  public isBlocked: boolean;

  @ApiProperty()
  @Column('text', { array: true })
  roles: string[];

  @ApiProperty()
  @Column({ nullable: true })
  status: string;

  @ManyToOne(() => UserEntity, { cascade: true, onDelete: 'CASCADE' })
  owner: UserEntity;

  @ApiProperty()
  @Column('text', { nullable: true })
  ownerId: string;

  toJSON() {
    return classToPlain(this);
  }

  toModel(data: string) {
    return plainToClass(OrganizationEntity, data);
  }
}
