import { ApiProperty } from '@nestjs/swagger';
import { classToPlain } from 'class-transformer';
import { Column, Entity, ManyToOne } from 'typeorm';
import { AbstractEntity } from '../shared/dtos/abstract.dto';
import { UserEntity } from './user.entity';

@Entity('cloth')
export class ClothEntity extends AbstractEntity {
  @ApiProperty()
  @Column('text')
  name: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  type: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  color: string;

  @ManyToOne(() => UserEntity, { cascade: true, onDelete: 'CASCADE' })
  owner: UserEntity;

  @ApiProperty()
  @Column('text', { nullable: true })
  ownerId: string;

  toJSON() {
    return classToPlain(this);
  }
}
