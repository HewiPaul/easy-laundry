import { ApiProperty } from '@nestjs/swagger';
import * as bcrypt from 'bcrypt';
import { classToPlain, Exclude, Expose, plainToClass } from 'class-transformer';
import { IsEmail } from 'class-validator';
import { BeforeInsert, Column, Entity, OneToMany } from 'typeorm';
import { AbstractEntity } from '../shared/dtos/abstract.dto';
import { ClothEntity } from './cloth.entity';
import { OrderEntity } from './order.entity';

@Entity('user')
export class UserEntity extends AbstractEntity {
  @ApiProperty()
  @Column('varchar', { length: 500 })
  public firstName: string;

  @ApiProperty()
  @Column('varchar', { length: 500, nullable: true })
  public lastName: string;

  @ApiProperty()
  @Column('varchar', { length: 500, unique: true })
  @IsEmail()
  public email: string;

  @ApiProperty()
  @Column('varchar', { length: 500 })
  @Exclude()
  public password: string;

  @ApiProperty()
  @Column()
  public isActive: boolean;

  @ApiProperty()
  @Column({ nullable: true })
  public phone: string;

  @ApiProperty()
  @Column()
  public isBlocked: boolean;

  @ApiProperty()
  @Column('text', { array: true })
  roles: string[];

  @ApiProperty()
  @Column({ nullable: true })
  profilePicture: string;

  @ApiProperty()
  @Column('timestamptz', { nullable: true })
  lastOnline: Date;

  @ApiProperty()
  @Column({ nullable: true })
  isOnline: boolean;

  @ApiProperty()
  @Column({ nullable: true })
  @Exclude()
  token: string;

  @ApiProperty()
  @Column({ nullable: true })
  @Exclude()
  passwordToken: string;

  @Column({ nullable: true })
  @Exclude()
  isRegisteredViaGoogle: boolean;

  @ApiProperty()
  @Column({ nullable: true })
  status: string;

  @ApiProperty({ nullable: true })
  clientId: string;

  @OneToMany(() => OrderEntity, (orders) => orders.owner)
  public userOrders!: OrderEntity[];

  @OneToMany(() => ClothEntity, (cloths) => cloths.owner)
  public userClothes!: OrderEntity[];

  @BeforeInsert()
  async hashPassword() {
    this.password = await bcrypt.hash(this.password, 10);
  }

  async hashPasswordForUpdate(password: string) {
    return await bcrypt.hash(password, 10);
  }

  async comparePassword(attempt: string) {
    return await bcrypt.compare(attempt, this.password);
  }

  toJSON() {
    return classToPlain(this);
  }

  toModel(data: string) {
    return plainToClass(UserEntity, data);
  }

  @Expose()
  get fullName(): string {
    return `${this.firstName ?? ''} ${this.lastName ?? ''}`;
  }
}

export interface AuthPayload {
  email: string;
}
