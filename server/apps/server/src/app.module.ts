import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { getMetadataArgsStorage } from 'typeorm';
import {
  utilities as nestWinstonModuleUtilities,
  WinstonModule,
} from 'nest-winston';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './features/auth/auth.module';
import winston from 'winston';
import { UserModule } from './features/user/user.module';
import { OrderModule } from './features/order/order.module';
import { ClothModule } from './features/cloth/cloth.module';
import { OrganizationModule } from './features/organization/organization.module';

@Module({
  imports: [
    AuthModule,
    UserModule,
    OrderModule,
    ClothModule,
    OrganizationModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secretOrPrivateKey: 'secretKey',
      signOptions: {
        expiresIn: 3600,
      },
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.DB_HOST,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_DATABASE,
      schema: process.env.DB_SCHEMA,
      entities: getMetadataArgsStorage().tables.map((tbl) => tbl.target),
      synchronize: process.env.ENV === 'DEV',
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
