import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { ClothEntity } from 'apps/server/src/entities/cloth.entity';
import { ClothesInOrder } from 'apps/server/src/entities/clothesinorder.entity';
import { OrderEntity } from 'apps/server/src/entities/order.entity';
import {
  IPaginationOptions,
  paginate,
  Pagination,
} from 'nestjs-typeorm-paginate';
import { getConnection } from 'typeorm';

@Injectable()
export class OrderService extends TypeOrmCrudService<OrderEntity> {
  constructor(@InjectRepository(OrderEntity) repo) {
    super(repo);
  }

  async getAll(options: IPaginationOptions): Promise<Pagination<OrderEntity>> {
    const queryBuilder = this.repo
      .createQueryBuilder('order')
      .leftJoinAndSelect('order.orderedClothes', 'orderedClothes')
      .leftJoinAndSelect('orderedClothes.cloth', 'clothes');
    queryBuilder.leftJoinAndSelect('order.owner', 'user').getOne();

    queryBuilder.orderBy('order.created', 'DESC');

    return paginate<OrderEntity>(queryBuilder, options);
  }

  async create(order: OrderEntity) {
    try {
      const newOrder = this.repo.create(order);
      const result = await newOrder.save();
      if (order.orderedClothes.length > 0) {
        const orderedClothes = await order.orderedClothes.map((o, i) => ({
          ...o,
          orderId: newOrder.id,
          totalPrice: o.unitPrice * o.quantity,
        }));
        await getConnection()
          .createQueryBuilder()
          .insert()
          .into(ClothesInOrder)
          .values(orderedClothes)
          .execute();
      }

      const { totalCost } = await getConnection()
        .getRepository(ClothesInOrder)
        .createQueryBuilder()
        .from(ClothesInOrder, 'clothes_in_order')
        .where('clothes_in_order.orderId = :orderId', {
          orderId: newOrder.id,
        })
        .select('SUM(clothes_in_order.totalPrice)', 'totalCost')
        .getRawOne();

      await getConnection()
        .createQueryBuilder()
        .update(OrderEntity)
        .set({
          totalCost: totalCost,
        })
        .where('id = :id', { id: newOrder.id })
        .execute();

      result.totalCost = totalCost;

      return { order: result, totalCost: totalCost };
    } catch (err) {
      throw new BadRequestException(err.message);
    }
  }

  async findById(id: string): Promise<OrderEntity> {
    const queryBuilder = this.repo.createQueryBuilder('order');
    queryBuilder
      .leftJoinAndSelect('order.owner', 'user')
      .where('order.id = :orderId', { orderId: id })
      .orderBy('order.created', 'DESC');

    return queryBuilder.getOne();
  }

  async update(id: string, data: OrderEntity) {
    const order = await this.findById(id);
    if (order) {
      await getConnection()
        .createQueryBuilder()
        .update(OrderEntity)
        .set({
          title: data.title,
          type: data.type,
          dueDate: data.dueDate,
        })
        .where('id = :id', { id: data.id })
        .execute();
    } else {
      throw new BadRequestException('Order not found!');
    }
    return this.findById(id);
  }

  async remove(_id) {
    try {
      await getConnection()
        .createQueryBuilder()
        .delete()
        .from('order')
        .where({
          id: _id,
        })
        .execute();

      return true;
    } catch (err) {
      throw new BadRequestException(err);
    }
  }
}
