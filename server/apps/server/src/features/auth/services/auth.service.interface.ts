import { UserEntity } from 'apps/server/src/entities/user.entity';
import { LoginDTO } from 'apps/server/src/shared/dtos/login.dto';
import { PasswordResetDto } from '../dtos/passwordResetDto';

export const AUTH_SERVICE = 'AUTH_SERVICE';

export interface IAuthService {
  googleLogin(req);
  register(credentials: UserEntity);
  resendEmail(credentials: PasswordResetDto);
  createAccessToken(user: UserEntity);
  createRefreshToken(user: UserEntity);
  validateUserToken(payload: UserEntity): Promise<UserEntity>;
  validateUser(email: string, password: string): Promise<UserEntity>;
  login({ email, password }: LoginDTO);
  activateUser(id: string, token: string);
  forgotPassword(email: string, redirectUrl: string);
  updateForgotPassword(id: string, token: string, password: string);
  validateRefreshToken(refreshToken: string);
}
