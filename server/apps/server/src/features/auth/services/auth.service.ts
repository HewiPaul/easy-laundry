import {
  BadRequestException,
  ConflictException,
  Injectable,
  Logger,
  NotAcceptableException,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from 'apps/server/src/entities/user.entity';
import { LoginDTO } from 'apps/server/src/shared/dtos/login.dto';
import { EmailDTO } from 'apps/server/src/shared/dtos/mail.dto';
import * as crypto from 'crypto';
import * as jwt from 'jsonwebtoken';
import { sign } from 'jsonwebtoken';
import { Repository } from 'typeorm';
import { UserService } from '../../user/services/user.service';
import { PasswordResetDto } from '../dtos/passwordResetDto';
import { sendEmail } from './sendemail';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserEntity) private userRepo: Repository<UserEntity>,
    private jwtService: JwtService,
    private readonly usersService: UserService,
  ) {}

  private logger: Logger = new Logger(AuthService.name);

  googleLogin(req) {
    if (!req.user) {
      return 'No user from google';
    }

    return {
      message: 'User information from google',
      user: req.user,
    };
  }

  async register(credentials: UserEntity) {
    try {
      credentials.email = credentials.email?.toLowerCase();
      const userExist = await this.userRepo.findOne({
        where: { email: credentials.email },
      });

      if (userExist == null) {
        const token = this.generateToken();

        credentials.token = token;
        const user = this.userRepo.create(credentials);
        await user.save();

        this.sendConfirmationEmail(credentials.clientId, user, token);

        return { ...user.toJSON() };
      } else if (userExist?.isRegisteredViaGoogle) {
        const id = userExist.id;
        userExist.password = await userExist.hashPasswordForUpdate(
          credentials.password,
        );
        userExist.firstName = credentials.firstName;
        userExist.lastName = credentials.lastName;
        userExist.isRegisteredViaGoogle = false;
        await this.userRepo.update({ id }, userExist);
        return userExist;
      } else {
        throw new ConflictException('Email has already been registered');
      }
    } catch (err) {
      if (err.code === '23505') {
        throw new ConflictException('Email has already been registered');
      }
      throw new BadRequestException(err);
    }
  }

  private generateToken() {
    return crypto.randomBytes(20).toString('hex');
  }

  async resendEmail({ email, clientId }: PasswordResetDto) {
    const user = await this.userRepo.findOne({
      email: email?.toLowerCase(),
      isActive: false,
    });

    if (!user) {
      throw new BadRequestException('Invalid Email!');
    }

    const token = this.generateToken();
    user.token = token;
    await this.userRepo.update({ id: user.id }, user);
    this.sendConfirmationEmail(clientId, user, token);
  }

  private sendConfirmationEmail(
    clientId: string,
    user: UserEntity,
    token: string,
  ) {
    let redirectUrl;

    if (clientId == null || clientId == '') redirectUrl = process.env.LINK;
    else if (clientId == 'bridger') redirectUrl = process.env.CHALLENGE;
    else if (clientId == 'presence') redirectUrl = process.env.LINK;

    const mail = new EmailDTO();
    if (clientId == 'bridger') {
      mail.subject = 'Bridger.Live Account Confirmation ✔';
      mail.message =
        "We're excited to have you get started. First, you need to confirm your account. Just press the button below.";
      mail.htmlHeader = `Dear ${user.firstName}, welcome to <span><span>Bridg<span><span style="color: #FF4073; text-decoration: underline; text-decoration-color: #53C2E8;">e</span>r</span>`;
      mail.htmlBodyTitle = 'We’re excited to have you get started. ';
      mail.htmlBody = 'Confirm your account to get started';
      mail.buttonName = 'Confirm Account';
    } else {
      mail.subject = 'Presence Browser Account Confirmation ✔';
      mail.message =
        "We're excited to have you get started. First, you need to confirm your account. Just press the button below.";
      mail.htmlHeader = `Dear ${user.firstName}, Welcome to Presence Browser!`;
      mail.htmlBody =
        "We're excited to have you get started. First, you need to confirm your account. Just press the button below.";
      mail.buttonName = 'Confirm Account';
    }

    sendEmail(
      user.email,
      user.firstName,
      `${redirectUrl}/account/activate/${user.id}/${token}?name=${
        user.fullName ?? ''
      }&email=${user.email ?? ''}`,
      mail,
    );
  }

  createAccessToken(user: UserEntity) {
    const expiresIn = 604800;

    const accessToken = jwt.sign(
      {
        id: user.id,
        email: user.email,
        firstName: user.firstName,
        lastName: user.lastName,
        roles: user.roles,
        fullName: user.firstName + ' ' + user.lastName,
        profilePicture: user.profilePicture,
      },
      process.env.SECRET_KEY,
      { expiresIn },
    );

    return {
      expiresIn,
      accessToken,
    };
  }

  createRefreshToken(user: UserEntity) {
    const expiresIn = 604800;

    const refreshToken = jwt.sign(
      {
        id: user.id,
        email: user.email,
        firstName: user.firstName,
        lastName: user.lastName,
        roles: user.roles,
        fullName: user.firstName + ' ' + user.lastName,
        profilePicture: user.profilePicture,
      },
      process.env.SECRET_KEY,
      { expiresIn },
    );

    return {
      expiresIn,
      refreshToken,
    };
  }

  async validateRefreshToken(refreshToken: string) {
    try {
      return await jwt.verify(refreshToken, process.env.SECRET_KEY);
    } catch (ex) {
      throw new BadRequestException(ex.message);
    }
  }

  async validateUserToken(payload: UserEntity): Promise<UserEntity> {
    return await this.userRepo.findOne(payload.id);
  }

  async validateOAuthLogin(
    thirdPartyId: string,
    provider: 'google',
    email: string,
    firstName: string,
    lastName: string,
  ): Promise<string> {
    try {
      const payload = {
        thirdPartyId,
        provider,
        email,
        firstName,
        lastName,
      };

      return sign(payload, process.env.GOOGLE_SECRET, {
        expiresIn: 3600,
      });
    } catch (err) {
      throw new BadRequestException('validateOAuthLogin', err.message);
    }
  }

  async validateUser(email: string, password: string): Promise<UserEntity> {
    const user = await this.usersService.findByEmail(email);
    if (user && user.comparePassword(password)) {
      this.logger.log('password check success');
      return user;
    }
    return null;
  }

  async login({ email, password }: LoginDTO) {
    const user = await this.userRepo.findOne({ email: email?.toLowerCase() });
    if (user != null) {
      const isValid = await user.comparePassword(password);
      const isActive = user.isActive;
      const isBlocked = user.isBlocked;
      if (!isValid) {
        throw new UnauthorizedException('Invalid username or password.');
      }
      if (!isActive) {
        throw new NotAcceptableException('Please confirm your email.');
      }
      if (isBlocked) {
        throw new NotAcceptableException(
          'Your Account is Blocked. Please Contact the System Admin.',
        );
      }
      const accessToken = this.createAccessToken(user);
      const refreshToken = this.createRefreshToken(user);
      user.token = accessToken.accessToken;
      return { user: user.toJSON(), accessToken, refreshToken };
    } else {
      throw new NotFoundException('Invalid username or password');
    }
  }

  async activateUser(id: string, token: string) {
    const user = await this.userRepo.findOne({
      where: { id: id, token: token },
    });
    if (user != null) {
      user.isActive = true;
      await this.userRepo.update({ id }, user);
      return user;
    } else {
      throw new NotAcceptableException('Invalid userId or token.');
    }
  }

  async forgotPassword(email: string, clientId: string) {
    const user = await this.userRepo.findOne({ where: { email } });

    if (!user) throw new NotFoundException('This email is not registered.');

    const token = this.createAccessToken(user);
    user.passwordToken = token.accessToken;
    user.token = token.accessToken;

    await user.save();

    let redirectUrl;

    if (clientId == null || clientId == '') redirectUrl = process.env.LINK;
    else if (clientId == 'bridger') redirectUrl = process.env.CHALLENGE;
    else if (clientId == 'presence') redirectUrl = process.env.LINK;

    const mail = new EmailDTO();

    if (clientId == 'bridger') {
      mail.subject = 'Bridger.Live Password Reset ✔';
      mail.message =
        'You recently requested to reset your password for your Bridger.Live Account. Click the button below to reset it.';
      mail.htmlHeader = `Dear ${user.firstName}, You Request Password Reset.`;
      mail.htmlBody =
        'You recently requested to reset your password for your Bridger.Live Account. Click the button below to reset it.';
      mail.buttonName = 'Reset Password';
    } else {
      mail.subject = 'Presence Browser Password Reset ✔';
      mail.message =
        'You recently requested to reset your password for your Presence Browser Account. Click the button below to reset it.';
      mail.htmlHeader = `Dear ${user.firstName}, You Request Password Reset.`;
      mail.htmlBody =
        'You recently requested to reset your password for your Presence Browser Account. Click the button below to reset it.';
      mail.buttonName = 'Reset Password';
    }

    await sendEmail(
      user.email,
      user.firstName,
      `${redirectUrl}/account/forgotPassword/${user.id}/${token.accessToken}`,
      mail,
    );
    return { user: { ...user.toJSON() } };
  }

  async updateForgotPassword(id: string, token: string, password: string) {
    const user = await this.userRepo.findOne({
      where: { id: id, passwordToken: token },
    });
    if (user != null) {
      user.password = await user.hashPasswordForUpdate(password);
      await this.userRepo.update({ id }, user);
      return user;
    } else {
      return new NotFoundException('user with this id or token not found!');
    }
  }
}
