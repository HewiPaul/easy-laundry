import {
  BadRequestException,
  Controller,
  Get,
  HttpStatus,
  Inject,
  Logger,
  LoggerService,
  Param,
  Req,
  Res,
  UnauthorizedException,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiTags } from '@nestjs/swagger';
import { UserEntity } from 'apps/server/src/entities/user.entity';
import { OAuth2Client } from 'google-auth-library';
import {
  IUserService,
  USER_SERVICE,
} from '../../user/services/user.service.interface';
import { AUTH_SERVICE, IAuthService } from '../services/auth.service.interface';
const client = new OAuth2Client(
  process.env.GOOGLE_CLIENT_ID,
  process.env.GOOGLE_SECRET,
  process.env.LINK + '/google/redirect',
);

@ApiTags('google')
@Controller('google')
export class GoogleAuthController {
  constructor(
    @Inject(Logger) private readonly logger: LoggerService,
    @Inject(AUTH_SERVICE)
    private readonly authService: IAuthService,
    @Inject(USER_SERVICE)
    private readonly userService: IUserService,
  ) {}

  @Get()
  @UseGuards(AuthGuard('google'))
  async googleAuth(@Req() req) {
    return req;
  }

  @Get('validate/:token')
  async validate(@Param('token') token: string, @Res() res): Promise<any> {
    try {
      const ticket = await client.verifyIdToken({
        idToken: token,
        audience: process.env.GOOGLE_CLIENT_ID,
      });

      const payload = await ticket.getPayload();

      const exist = await this.userService.findByEmail(payload.email);
      if (exist) {
        return res.status(HttpStatus.OK).json({
          user: exist,
          token: this.authService.createAccessToken(exist),
        });
      } else {
        const newUser = new UserEntity();
        newUser.email = payload.email;
        newUser.firstName = payload.given_name;
        newUser.lastName = payload.family_name;
        newUser.password = Math.floor(Math.random() * 8).toString();
        newUser.roles = [];
        newUser.roles.push('user');
        newUser.isActive = false;
        newUser.isBlocked = false;
        newUser.isRegisteredViaGoogle = true;
        newUser.isOnline = false;
        newUser.status = 'public';

        const registeredUser = await this.authService.register(newUser);

        newUser.token = this.authService.createAccessToken(registeredUser);
        return res.status(HttpStatus.OK).json({
          user: newUser,
          token: newUser.token,
        });
      }
    } catch (ex) {
      throw new UnauthorizedException(ex.message);
    }
  }

  @Get('redirect')
  @UseGuards(AuthGuard('google'))
  async googleLoginCallback(@Req() req, @Res() res) {
    // handles the Google OAuth2 callback
    try {
      //
      const jwt = req.user.jwt;
      const email = req.user.email;

      const exist = await this.userService.findByEmail(email);
      if (exist) {
        if (jwt) {
          return res.status(HttpStatus.OK).json({
            user: exist,
            token: jwt,
          });
        } else throw new UnauthorizedException('Unauthorized request.');
      } else {
        if (jwt) {
          const newUser = new UserEntity();
          newUser.email = req.user.email;
          newUser.firstName = req.user.firstName;
          newUser.lastName = req.user.lastName;
          newUser.password = Math.floor(Math.random() * 8).toString();
          newUser.roles = [];
          newUser.roles.push('user');
          newUser.isActive = false;
          newUser.isBlocked = false;
          newUser.isRegisteredViaGoogle = true;
          newUser.isOnline = false;
          newUser.status = 'public';

          await this.authService.register(newUser);

          newUser.token = jwt;
          return res.status(HttpStatus.OK).json({
            user: newUser,
            token: jwt,
          });
        }
      }
    } catch (ex) {
      return new BadRequestException(ex);
    }
  }
}
