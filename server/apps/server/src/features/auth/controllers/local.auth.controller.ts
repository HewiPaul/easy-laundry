import {
  BadRequestException,
  Body,
  Controller,
  Get,
  HttpStatus,
  Inject,
  Logger,
  LoggerService,
  NotAcceptableException,
  Param,
  Post,
  Put,
  Res,
  UnauthorizedException,
  ValidationPipe,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { UserEntity } from 'apps/server/src/entities/user.entity';
import { LoginDTO } from 'apps/server/src/shared/dtos/login.dto';
import { ForgotPasswordDTO } from 'apps/server/src/shared/dtos/password.dto';
import { Response } from 'express';
import { OAuth2Client } from 'google-auth-library';
import { User } from '../../user/decorators/user.decorator';
import {
  IUserService,
  USER_SERVICE,
} from '../../user/services/user.service.interface';
import { PasswordResetDto } from '../dtos/passwordResetDto';
import { AUTH_SERVICE, IAuthService } from '../services/auth.service.interface';

const client = new OAuth2Client(
  process.env.GOOGLE_CLIENT_ID,
  process.env.GOOGLE_SECRET,
  process.env.LINK + '/google/redirect',
);
@ApiTags('account')
@Controller('account')
export class AuthController {
  constructor(
    @Inject(Logger) private readonly logger: LoggerService,
    @Inject(AUTH_SERVICE)
    private readonly authService: IAuthService,
    @Inject(USER_SERVICE)
    private readonly usersService: IUserService,
  ) {}

  @Post('/signup')
  public async register(@Body(ValidationPipe) credentials: UserEntity) {
    credentials.roles = [];
    credentials.roles.push('user');
    credentials.isActive = false;
    credentials.isBlocked = false;
    credentials.isRegisteredViaGoogle = false;
    credentials.isOnline = false;
    credentials.status = 'public';
    return this.authService.register(credentials);
  }

  @Post('/resend')
  public async resend(@Body(ValidationPipe) credentials: PasswordResetDto) {
    return this.authService.resendEmail(credentials);
  }

  @Post('/login')
  public async login(
    @Res() res: Response,
    @Body(ValidationPipe) credentials: LoginDTO,
  ) {
    const user = await this.usersService.findByEmail(credentials.email);
    if (!user) {
      res.status(HttpStatus.NOT_FOUND).json({
        message: 'Invalid username or password.',
      });
    } else {
      try {
        const token = await this.authService.login(credentials);
        return res.status(HttpStatus.OK).json(token);
      } catch (ex) {
        res.status(HttpStatus.UNAUTHORIZED).json({
          message: ex.message,
        });
      }
    }
  }

  @Post('googleLogin/:token')
  async googleLogin(@Param('token') token: string, @Res() res): Promise<any> {
    try {
      const ticket = await client.verifyIdToken({
        idToken: token,
        audience: process.env.GOOGLE_CLIENT_ID,
      });

      const payload = ticket.getPayload();

      const exist = await this.usersService.findByEmail(payload.email);
      if (exist) {
        if (exist.isBlocked) {
          throw new NotAcceptableException(
            'Your Account is Blocked. Please Contact the System Admin.',
          );
        }

        return res.status(HttpStatus.OK).json({
          user: exist,
          accessToken: this.authService.createAccessToken(exist),
          refreshToken: this.authService.createRefreshToken(exist),
        });
      } else {
        const newUser = new UserEntity();
        newUser.email = payload.email;
        newUser.firstName = payload.given_name;
        newUser.lastName = payload.family_name;
        newUser.password = Math.floor(Math.random() * 8).toString();
        newUser.roles = [];
        newUser.roles.push('user');
        newUser.isActive = false;
        newUser.isBlocked = false;
        newUser.isRegisteredViaGoogle = true;
        newUser.isOnline = false;
        newUser.status = 'public';

        const registeredUser = await this.authService.register(newUser);

        newUser.token = this.authService.createAccessToken(registeredUser);
        return res.status(HttpStatus.OK).json({
          user: newUser,
          accessToken: this.authService.createAccessToken(registeredUser),
          refreshToken: this.authService.createRefreshToken(registeredUser),
        });
      }
    } catch (ex) {
      throw new UnauthorizedException(ex.message);
    }
  }

  @Put('activate/:id/:token')
  activateUser(@Param('id') id: string, @Param('token') token: string) {
    return this.authService.activateUser(id, token);
  }

  @Post('forgotPassword')
  forgotPassword(@Body(ValidationPipe) data: PasswordResetDto) {
    return this.authService.forgotPassword(data.email, data.clientId);
  }

  @Put('updateForgotPassword/:id/:token')
  async updateForgotPassword(
    @Param('id') id: string,
    @Param('token') token: string,
    @Body(ValidationPipe) password: ForgotPasswordDTO,
  ) {
    try {
      return await this.authService.updateForgotPassword(
        id,
        token,
        password.password,
      );
    } catch (ex) {
      return new BadRequestException(ex);
    }
  }

  @Get('refreshToken/:token')
  async refreshToken(
    @User() user,
    @Param('token') token: string,
    @Res() res: Response,
  ) {
    try {
      const _user = await this.usersService.findById(user.id);
      const isRefreshTokenMatching =
        await this.authService.validateRefreshToken(token);

      if (isRefreshTokenMatching && _user.isActive && !_user.isBlocked) {
        return res.status(HttpStatus.OK).json({
          user: _user,
          accessToken: this.authService.createAccessToken(user),
          refreshToken: this.authService.createRefreshToken(user),
        });
      } else {
        throw new UnauthorizedException(
          'Invalid refreshToken detected or user is blocked. please contact system admin.',
        );
      }
    } catch (ex) {
      throw new UnauthorizedException(ex.message);
    }
  }
}
