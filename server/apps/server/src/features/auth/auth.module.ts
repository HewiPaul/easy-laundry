import { Logger, Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from '../../entities/user.entity';
import { UserService } from '../user/services/user.service';
import { USER_SERVICE } from '../user/services/user.service.interface';
import { UserModule } from '../user/user.module';
import { AuthController } from './controllers/local.auth.controller';
import { AuthService } from './services/auth.service';
import { AUTH_SERVICE } from './services/auth.service.interface';
import { GoogleStrategy } from './strategies/google.strategy';
import { JwtStrategy } from './strategies/jwt.strategy';
import { LocalStrategy } from './strategies/local.strategy';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt', property: 'user' }),
    TypeOrmModule.forFeature([UserEntity]),
    JwtModule.register({
      secret: process.env.SECRET_KEY,
      signOptions: {
        expiresIn: 3600,
      },
    }),
    UserEntity,
    UserModule,
  ],
  providers: [
    AuthService,
    GoogleStrategy,
    JwtStrategy,
    LocalStrategy,
    UserService,
    Logger,
    {
      useClass: UserService,
      provide: USER_SERVICE,
    },
    {
      useClass: AuthService,
      provide: AUTH_SERVICE,
    },
  ],
  controllers: [AuthController],
  exports: [PassportModule, JwtStrategy],
})
export class AuthModule {}
