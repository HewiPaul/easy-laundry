import {
  createParamDecorator,
  ExecutionContext,
  UnauthorizedException,
} from '@nestjs/common';
import { UserEntity } from 'apps/server/src/entities/user.entity';
import jwt_decode from 'jwt-decode';

export const User = createParamDecorator(
  (data: string, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();

    if (request.headers?.authorization != null) {
      const token = request.headers?.authorization;
      return jwt_decode<UserEntity>(token);
    } else {
      throw new UnauthorizedException('Unauthorized request.');
    }
  }
);

export const PublicUser = createParamDecorator(
  (data: string, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();

    if (request.headers?.authorization != null) {
      const token = request.headers?.authorization;
      return jwt_decode<UserEntity>(token);
    } else {
      return null;
    }
  }
);
