import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Inject,
  Logger,
  LoggerService,
  Param,
  Post,
  Put,
  Query,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors,
  ValidationPipe,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { UserEntity } from 'apps/server/src/entities/user.entity';
import { ChangePasswordDTO } from 'apps/server/src/shared/dtos/password.dto';
import { AllowedRoles } from 'apps/server/src/shared/dtos/role.enum';
import * as fs from 'fs';
import { diskStorage } from 'multer';
import * as validateUUID from 'uuid-validate';
import { Auth } from '../../auth/decorators/auth.decorator';
import { JwtAuthGuard } from '../../auth/guards/jwt-auth.guard';
import { Public } from '../../auth/guards/public.auth.guard';
import { RolesGuard } from '../../auth/guards/roles.guard';
import { editFileName } from '../decorators/filename.decorator';
import { User } from '../decorators/user.decorator';
import { IUserService, USER_SERVICE } from '../services/user.service.interface';

@UseGuards(JwtAuthGuard, RolesGuard)
@ApiBearerAuth()
@ApiTags('user')
@Controller('user')
export class UserController {
  constructor(
    @Inject(Logger) private readonly logger: LoggerService,
    @Inject(USER_SERVICE)
    private readonly service: IUserService,
  ) {}

  @Get('getCurrentUser')
  async GetCurrentUser(@User() user) {
    try {
      return await this.service.findByEmail(user.email);
    } catch (ex) {
      return new BadRequestException(ex);
    }
  }

  @Get('getAll')
  @Auth(AllowedRoles.admin)
  async GetAll(@Query('page') page = 1, @Query('limit') limit = 100) {
    try {
      //const users = await this.service.findAll();
      limit = limit > 100 ? 100 : limit;
      return await this.service.paginate({
        page,
        limit,
      });
    } catch (ex) {
      return new BadRequestException(ex);
    }
  }

  @Get('findByEmail/:email')
  async findByEmail(@Param('email') email: string) {
    try {
      return await this.service.findByEmail(email);
    } catch (ex) {
      return new BadRequestException(ex);
    }
  }

  @Get('findById/:id')
  async findById(@Param('id') id: string) {
    try {
      return await this.service.findById(id);
    } catch (ex) {
      return new BadRequestException(ex);
    }
  }

  @Get('activeUsers')
  async getActiveUsers(@Query('page') page = 1, @Query('limit') limit = 100) {
    try {
      //const users = await this.service.findAll();
      limit = limit > 100 ? 100 : limit;
      return await this.service.getActiveUsers({
        page,
        limit,
      });
      //return res.status(HttpStatus.OK).json(users);
    } catch (ex) {
      return new BadRequestException(ex);
    }
  }

  @Get('inActiveUsers')
  async getInActiveUsers(@Query('page') page = 1, @Query('limit') limit = 100) {
    try {
      //const users = await this.service.findAll();
      limit = limit > 100 ? 100 : limit;
      return await this.service.getInActiveUsers({
        page,
        limit,
      });
      //return res.status(HttpStatus.OK).json(users);
    } catch (ex) {
      return new BadRequestException(ex);
    }
  }

  @Get('blockedUsers')
  async getBlockedUsers(@Query('page') page = 1, @Query('limit') limit = 100) {
    try {
      //const users = await this.service.findAll();
      limit = limit > 100 ? 100 : limit;
      return await this.service.getBlockedUsers({
        page,
        limit,
      });
      //return res.status(HttpStatus.OK).json(users);
    } catch (ex) {
      return new BadRequestException(ex);
    }
  }

  @Get('getOnlineUsers')
  async getOnlineUsers() {
    try {
      return await this.service.getOnlineUsers();
    } catch (ex) {
      return new BadRequestException(ex);
    }
  }

  @Get('getOfflineUsers')
  async getOfflineUsers() {
    try {
      return await this.service.getOfflineUsers();
    } catch (ex) {
      return new BadRequestException(ex);
    }
  }

  @Put('update/:id')
  async update(
    @Param('id') id: string,
    @Body(ValidationPipe) data: UserEntity,
  ) {
    try {
      return await this.service.updateUser(id, data);
    } catch (ex) {
      return new BadRequestException(ex);
    }
  }

  @Put('deactivate/:id')
  @Auth(AllowedRoles.admin)
  async deactivateUser(@Param('id') id: string) {
    try {
      return await this.service.deactivateUser(id);
    } catch (ex) {
      return new BadRequestException(ex);
    }
  }

  @Put('block/:id')
  @Auth(AllowedRoles.admin)
  async blockUser(@Param('id') id: string) {
    try {
      return await this.service.blockUser(id);
    } catch (ex) {
      return new BadRequestException(ex);
    }
  }

  @Put('unblock/:id')
  @Auth(AllowedRoles.admin)
  async unblockUser(@Param('id') id: string) {
    try {
      return await this.service.unblockUser(id);
    } catch (ex) {
      return new BadRequestException(ex);
    }
  }

  @Put('changePassword')
  async changePassword(
    @User() user,
    @Body(ValidationPipe) password: ChangePasswordDTO,
  ) {
    try {
      return await this.service.updateChangedPassword(
        user.id,
        password.oldPassword,
        password.newPassword,
      );
    } catch (ex) {
      return new BadRequestException(ex);
    }
  }

  @Put('changeUserRole/:id')
  async changeUserRole(
    @Param('id') id: string,
    @Body(ValidationPipe) data: string[],
  ) {
    try {
      return await this.service.updateUserRole(id, data);
    } catch (ex) {
      return new BadRequestException(ex);
    }
  }

  @Put('setUserOnline/:id')
  async setUserOnline(@Param('id') id: string) {
    try {
      return await this.service.setOnline(id);
    } catch (ex) {
      return new BadRequestException(ex);
    }
  }

  @Get('checkOnlineStatus/:id')
  async checkOnlineStatus(@Param('id') id: string) {
    try {
      return await this.service.checkOnlineStatus(id);
    } catch (ex) {
      return new BadRequestException(ex);
    }
  }

  @Post('uploadProfilePicture')
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: process.env.UPLOAD_ROOT_PATH,
        filename: editFileName,
      }),
    }),
  )
  async uploadProfilePicture(@User() user, @UploadedFile() file) {
    try {
      const validFileExtensions = ['.jpg', '.jpeg', '.bmp', '.gif', '.png'];
      const extension = '.' + file.filename.split('.').pop();
      if (!validFileExtensions.includes(extension)) {
        throw new BadRequestException('file should be an image type.');
      }
      await this.service.uploadProfilePicture(user.id, file.filename);
      return {
        originalname: file.originalname,
        filename: file.filename,
      };
    } catch (ex) {
      return new BadRequestException(ex.message);
    }
  }

  @Get('getMyProfilePicture')
  async getMyProfilePicture(@User() user, @Res() res) {
    try {
      const user_ = await this.service.findById(user.id);
      if (user_.profilePicture == null) {
        return res.status(200).send(user_);
      } else {
        if (
          !fs.existsSync(
            process.env.UPLOAD_ROOT_PATH + '/' + user_.profilePicture,
          )
        ) {
          return res
            .status(404)
            .send('Image Not found in the specified directory!');
        }
        return res.sendFile(user_.profilePicture, {
          root: process.env.UPLOAD_ROOT_PATH,
        });
      }
    } catch (ex) {
      return new BadRequestException(ex);
    }
  }

  @Public()
  @Get('getProfilePicture/:id')
  async getProfilePicture(@Param('id') id: string, @Res() res) {
    try {
      if (!validateUUID(id)) {
        return res.status(404).send('Invalid Guid Detected!');
      }
      const user = await this.service.findById(id);
      if (user != null) {
        if (user.profilePicture == null) {
          return res.status(200).send(user);
        } else {
          if (
            !fs.existsSync(
              process.env.UPLOAD_ROOT_PATH + '/' + user.profilePicture,
            )
          ) {
            return res
              .status(404)
              .send('Image Not found in the specified directory!');
          }
          return res.sendFile(user.profilePicture, {
            root: process.env.UPLOAD_ROOT_PATH,
          });
        }
      } else {
        return res.status(404).send('User Not found!');
      }
    } catch (ex) {
      return new BadRequestException(ex.message);
    }
  }

  @Post('removeProfilePicture/:id')
  async removeProfilePicture(@Param('id') id: string, @Res() res) {
    try {
      const user = await this.service.findById(id);
      fs.unlinkSync(process.env.UPLOAD_ROOT_PATH + '/' + user.profilePicture);
      await this.service.removeProfilePicture(id);
      return res.status(200).send('User Picture Removed Successfully!');
    } catch (ex) {
      return res
        .status(400)
        .send('User Profile Picture Removal Failed: ' + ex.message);
    }
  }

  @Post('setStatus/:status')
  async setStatus(@User() user, @Param('status') status: string) {
    try {
      return await this.service.setUserStatus(user.id, status);
    } catch (ex) {
      return new BadRequestException(ex);
    }
  }
}
