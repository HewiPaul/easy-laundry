import { Logger, Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from '../../entities/user.entity';
import { UserController } from './controllers/user.controller';
import { UserService } from './services/user.service';
import { USER_SERVICE } from './services/user.service.interface';

export const passportModule = PassportModule.register({
  defaultStrategy: 'jwt',
});

@Module({
  imports: [TypeOrmModule.forFeature([UserEntity]), passportModule],
  controllers: [UserController],
  providers: [
    Logger,
    {
      useClass: UserService,
      provide: USER_SERVICE,
    },
  ],
})
export class UserModule {}
