import { UserEntity } from 'apps/server/src/entities/user.entity';
import { IPaginationOptions, Pagination } from 'nestjs-typeorm-paginate';

export const USER_SERVICE = 'USER_SERVICE';

export interface IUserService {
  paginate(options: IPaginationOptions): Promise<Pagination<UserEntity>>;
  findAll(options: IPaginationOptions): Promise<Pagination<UserEntity>>;
  findByEmail(email: string): Promise<UserEntity>;
  findById(id: string): Promise<UserEntity>;
  findByIdWithNoDetail(id_: string);
  getActiveUsers(options: IPaginationOptions): Promise<Pagination<UserEntity>>;
  getActiveUsersAsync(): Promise<UserEntity[]>;
  getInActiveUsers(
    options: IPaginationOptions,
  ): Promise<Pagination<UserEntity>>;
  getBlockedUsers(options: IPaginationOptions): Promise<Pagination<UserEntity>>;
  getOnlineUsers(): Promise<UserEntity[]>;
  getOfflineUsers(): Promise<UserEntity[]>;
  updateUser(id: string, data: UserEntity);
  updateUserRole(id: string, data: string[]);
  activateUser(id: string, token: string);
  blockUser(id: string);
  deactivateUser(id: string);
  unblockUser(id: string);
  updateChangedPassword(id: string, oldPassword: string, newPassword: string);
  uploadProfilePicture(id: string, path: string);
  setOnline(id: string);
  checkOnlineStatus(id: string);
  removeProfilePicture(id: string);
  setUserStatus(id: string, status: string);
}
