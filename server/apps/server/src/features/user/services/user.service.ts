import { Injectable, NotAcceptableException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { UserEntity } from 'apps/server/src/entities/user.entity';
import {
  IPaginationOptions,
  paginate,
  Pagination,
} from 'nestjs-typeorm-paginate';
import { IUserService } from './user.service.interface';

@Injectable()
export class UserService
  extends TypeOrmCrudService<UserEntity>
  implements IUserService
{
  private readonly isUserBlocked = 'user.isBlocked = :isBlocked';

  private readonly userCreated = 'user.created';

  constructor(@InjectRepository(UserEntity) repo) {
    super(repo);
  }

  async paginate(options: IPaginationOptions): Promise<Pagination<UserEntity>> {
    const queryBuilder = this.repo.createQueryBuilder('user');
    queryBuilder;
    queryBuilder
      .leftJoinAndSelect('user.userOrders', 'userOrders')
      .leftJoinAndSelect('userOrders.orderedClothes', 'orderedClothes')
      .leftJoinAndSelect('orderedClothes.cloth', 'clothes')
      .orderBy(this.userCreated, 'DESC'); // Or whatever you need to do

    return paginate<UserEntity>(queryBuilder, options);
  }

  async findAll(options: IPaginationOptions): Promise<Pagination<UserEntity>> {
    const queryBuilder = this.repo
      .createQueryBuilder('user')
      .orderBy(this.userCreated, 'DESC'); // Or whatever you need to do

    return paginate<UserEntity>(queryBuilder, options);
  }

  async findByEmail(_email: string): Promise<UserEntity> {
    const queryBuilder = this.repo
      .createQueryBuilder('user')
      .where('user.email = :email', { email: _email });

    return queryBuilder.getOne();
  }

  async findById(id_: string): Promise<UserEntity> {
    const queryBuilder = await this.repo
      .createQueryBuilder('user')
      .where('user.id = :id', { id: id_ });

    return await queryBuilder.getOne();
  }

  async findByIdWithNoDetail(id_: string): Promise<UserEntity> {
    const queryBuilder = await this.repo
      .createQueryBuilder('user')
      .where('user.id = :id', { id: id_ });

    return await queryBuilder.getOne();
  }

  async getActiveUsers(
    options: IPaginationOptions,
  ): Promise<Pagination<UserEntity>> {
    const queryBuilder = this.repo
      .createQueryBuilder('user')
      .where('user.isActive = :isActive', { isActive: true })
      .andWhere(this.isUserBlocked, { isBlocked: false })
      .orderBy(this.userCreated, 'DESC'); // Or whatever you need to do

    return paginate<UserEntity>(queryBuilder, options);
  }

  async getActiveUsersAsync(): Promise<UserEntity[]> {
    const queryBuilder = this.repo
      .createQueryBuilder('user')
      .where('user.isActive = :isActive', { isActive: true })
      .andWhere(this.isUserBlocked, { isBlocked: false })
      .orderBy(this.userCreated, 'DESC'); // Or whatever you need to do

    return queryBuilder.getMany();
  }

  async getInActiveUsers(
    options: IPaginationOptions,
  ): Promise<Pagination<UserEntity>> {
    const queryBuilder = this.repo
      .createQueryBuilder('user')
      .where('user.isActive = :isActive', { isActive: false })
      .orWhere(this.isUserBlocked, { isBlocked: true })
      .orderBy(this.userCreated, 'DESC'); // Or whatever you need to do

    return paginate<UserEntity>(queryBuilder, options);
  }

  async getBlockedUsers(
    options: IPaginationOptions,
  ): Promise<Pagination<UserEntity>> {
    const queryBuilder = this.repo
      .createQueryBuilder('user')
      .where(this.isUserBlocked, { isBlocked: true })
      .orderBy(this.userCreated, 'DESC'); // Or whatever you need to do

    return paginate<UserEntity>(queryBuilder, options);
  }

  async getOnlineUsers(): Promise<UserEntity[]> {
    const queryBuilder = this.repo
      .createQueryBuilder('user')
      .where('user.isOnline = :isOnline', { isOnline: true });

    return queryBuilder.getMany();
  }

  async getOfflineUsers(): Promise<UserEntity[]> {
    const queryBuilder = this.repo
      .createQueryBuilder('user')
      .where('user.isOnline = :isOnline', { isOnline: false });

    return queryBuilder.getMany();
  }

  async updateUser(id: string, data: UserEntity) {
    const user = await this.repo.findOne({ where: { id } });
    user.firstName = data.firstName;
    user.lastName = data.lastName;
    user.roles = data.roles;

    await this.repo.update({ id }, user);
    return this.findById(id);
  }

  async updateUserRole(id: string, data: string[]) {
    const user = await this.repo.findOne({ where: { id } });
    user.roles = data;
    await this.repo.update({ id }, user);
    return this.findById(id);
  }

  async setOnline(id: string) {
    const user = await this.repo.findOne({ where: { id } });
    user.lastOnline = new Date();
    user.isOnline = true;
    await this.repo.update({ id }, user);
    return this.findById(id);
  }

  async checkOnlineStatus(id: string) {
    const user = await this.repo.findOne({ where: { id } });

    const minutesToSubtract = 2;
    const currentDate = new Date();

    const limit = new Date(currentDate.getTime() - minutesToSubtract * 60000);

    if (user.lastOnline > limit)
      return {
        userId: user.id,
        userEmail: user.email,
        isOnline: true,
        lastOnline: user.lastOnline,
      };
    else {
      user.isOnline = false;
      await this.repo.update({ id }, user);
      return {
        userId: user.id,
        userEmail: user.email,
        isOnline: false,
        lastOnline: user.lastOnline,
      };
    }
  }

  async activateUser(id: string, token: string) {
    const user = await this.repo.findOne({ where: { id: id, token: token } });
    if (user != null) {
      user.isActive = true;
      await this.repo.update({ id }, user);
      return user;
    } else {
      throw new NotAcceptableException('Invalid userId or token used.');
    }
  }

  async blockUser(id: string) {
    const user = await this.repo.findOne({ where: { id } });
    user.isBlocked = true;
    await this.repo.update({ id }, user);
    return user;
  }

  async deactivateUser(id: string) {
    const user = await this.repo.findOne({ where: { id } });
    user.isActive = false;
    await this.repo.update({ id }, user);
    return user;
  }

  async unblockUser(id: string) {
    const user = await this.repo.findOne({ where: { id } });
    user.isBlocked = false;
    await this.repo.update({ id }, user);
    return user;
  }

  async updateForgotPassword(id: string, token: string, password: string) {
    const user = await this.repo.findOne({
      where: { id: id, passwordToken: token },
    });
    user.password = await user.hashPasswordForUpdate(password);
    await this.repo.update({ id }, user);
    return this.findById(id);
  }

  async updateChangedPassword(
    id: string,
    oldPassword: string,
    newPassword: string,
  ) {
    const user = await this.repo.findOne({
      where: { id: id },
    });
    const isOldPasswordValid = await user.comparePassword(oldPassword);
    if (isOldPasswordValid) {
      user.password = await user.hashPasswordForUpdate(newPassword);
      await this.repo.update({ id }, user);
      return this.findById(id);
    } else {
      throw new NotAcceptableException('Invalid Old Password!');
    }
  }

  async uploadProfilePicture(id: string, path: string) {
    const user = await this.repo.findOne({
      where: { id: id },
    });
    user.profilePicture = path;
    await this.repo.update({ id }, user);
    return this.findById(id);
  }

  async removeProfilePicture(id: string) {
    const user = await this.repo.findOne({
      where: { id: id },
    });
    user.profilePicture = null;
    await this.repo.update({ id }, user);
    return this.findById(id);
  }

  async setUserStatus(id: string, status: string) {
    const user = await this.repo.findOne({ where: { id } });
    user.status = status;
    await this.repo.update({ id }, user);
    return user;
  }
}
