import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClothEntity } from '../../entities/cloth.entity';
import { ClothController } from './controllers/cloth.controller';
import { ClothService } from './services/cloth.service';

@Module({
  imports: [TypeOrmModule.forFeature([ClothEntity])],
  controllers: [ClothController],
  providers: [ClothService],
})
export class ClothModule {}
