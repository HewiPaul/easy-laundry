import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { ClothEntity } from 'apps/server/src/entities/cloth.entity';
import * as crypto from 'crypto';
import * as jwt from 'jsonwebtoken';
import {
  IPaginationOptions,
  paginate,
  Pagination,
} from 'nestjs-typeorm-paginate';
import { getConnection } from 'typeorm';

@Injectable()
export class ClothService extends TypeOrmCrudService<ClothEntity> {
  constructor(@InjectRepository(ClothEntity) repo) {
    super(repo);
  }

  async getAll(options: IPaginationOptions): Promise<Pagination<ClothEntity>> {
    const queryBuilder = this.repo.createQueryBuilder('cloth');
    queryBuilder.leftJoinAndSelect('cloth.owner', 'user').getOne();

    queryBuilder.orderBy('cloth.created', 'DESC');

    return paginate<ClothEntity>(queryBuilder, options);
  }

  async create(cloth: ClothEntity) {
    try {
      const newCloth = this.repo.create(cloth);
      await newCloth.save();
      return { cloth: await this.findById(newCloth.id) };
    } catch (err) {
      throw new BadRequestException(err.message);
    }
  }

  async findById(id: string): Promise<ClothEntity> {
    const queryBuilder = this.repo.createQueryBuilder('cloth');
    queryBuilder
      .leftJoinAndSelect('cloth.owner', 'user')
      .where('cloth.id = :clothId', { clothId: id })
      .orderBy('cloth.created', 'DESC');

    return queryBuilder.getOne();
  }

  async update(id: string, data: ClothEntity) {
    const cloth = await this.findById(id);
    if (cloth) {
      await getConnection()
        .createQueryBuilder()
        .update(ClothEntity)
        .set({
          name: data.name,
          type: data.type,
          color: data.color,
        })
        .where('id = :id', { id: data.id })
        .execute();
    } else {
      throw new BadRequestException('Cloth not found!');
    }
    return this.findById(id);
  }

  async remove(_id) {
    try {
      await getConnection()
        .createQueryBuilder()
        .delete()
        .from('cloth')
        .where({
          id: _id,
        })
        .execute();

      return true;
    } catch (err) {
      throw new BadRequestException(err);
    }
  }
}
