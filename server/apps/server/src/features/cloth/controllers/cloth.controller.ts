import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { ClothEntity } from 'apps/server/src/entities/cloth.entity';
import { JwtAuthGuard } from '../../auth/guards/jwt-auth.guard';
import { RolesGuard } from '../../auth/guards/roles.guard';
import { User } from '../../user/decorators/user.decorator';
import { ClothService } from '../services/cloth.service';

@UseGuards(JwtAuthGuard, RolesGuard)
@ApiBearerAuth()
@ApiTags('cloth')
@Controller('cloth')
export class ClothController {
  constructor(public service: ClothService) {}

  @Post('create')
  register(@User() user, @Body(ValidationPipe) cloth: ClothEntity) {
    cloth.owner = user;
    return this.service.create(cloth);
  }

  @Get('getAll')
  async GetAll(@Query('page') page = 1, @Query('limit') limit = 100) {
    try {
      limit = limit > 100 ? 100 : limit;
      return await this.service.getAll({
        page,
        limit,
      });
    } catch (ex) {
      return new BadRequestException(ex);
    }
  }

  @Get('findById/:id')
  findById(@Param('id') id: string) {
    return this.service.findById(id);
  }

  @Put('update/:id')
  update(@Param('id') id: string, @Body(ValidationPipe) data: ClothEntity) {
    return this.service.update(id, data);
  }

  @Delete('delete/:id')
  delete(@Param('id') id: string) {
    return this.service.remove(id);
  }
}
