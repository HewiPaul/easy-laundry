import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { OrderEntity } from 'apps/server/src/entities/order.entity';
import { OrganizationEntity } from 'apps/server/src/entities/organization.entity';
import {
  IPaginationOptions,
  paginate,
  Pagination,
} from 'nestjs-typeorm-paginate';
import { getConnection } from 'typeorm';

@Injectable()
export class OrganizationService extends TypeOrmCrudService<OrganizationEntity> {
  constructor(@InjectRepository(OrganizationEntity) repo) {
    super(repo);
  }

  async getAll(
    options: IPaginationOptions,
  ): Promise<Pagination<OrganizationEntity>> {
    const queryBuilder = this.repo.createQueryBuilder('organization');
    queryBuilder.leftJoinAndSelect('organization.owner', 'user').getOne();

    queryBuilder.orderBy('order.created', 'DESC');

    return paginate<OrganizationEntity>(queryBuilder, options);
  }

  async create(organization: OrganizationEntity) {
    try {
      const newOrganization = this.repo.create(organization);
      await newOrganization.save();
      return { order: await this.findById(newOrganization.id) };
    } catch (err) {
      throw new BadRequestException(err.message);
    }
  }

  async findById(id: string): Promise<OrganizationEntity> {
    const queryBuilder = this.repo.createQueryBuilder('organization');
    queryBuilder
      .leftJoinAndSelect('organization.owner', 'user')
      .where('organization.id = :organizationId', { organizationId: id })
      .orderBy('organization.created', 'DESC');

    return queryBuilder.getOne();
  }

  async update(id: string, data: OrganizationEntity) {
    const order = await this.findById(id);
    if (order) {
      await getConnection()
        .createQueryBuilder()
        .update(OrganizationEntity)
        .set({
          name: data.name,
          shortName: data.shortName,
          type: data.type,
          location: data.location,
          roles: data.roles,
          status: status,
          //phones: data.phones,
        })
        .where('id = :id', { id: data.id })
        .execute();
    } else {
      throw new BadRequestException('Organization not found!');
    }
    return this.findById(id);
  }

  async remove(_id) {
    try {
      await getConnection()
        .createQueryBuilder()
        .delete()
        .from('organization')
        .where({
          id: _id,
        })
        .execute();

      return true;
    } catch (err) {
      throw new BadRequestException(err);
    }
  }
}
