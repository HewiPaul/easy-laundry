export class EmailDTO {
  subject: string;
  message: string;
  htmlHeader: string;
  htmlBodyTitle: string;
  htmlBody: string;
  buttonName: string;
}
