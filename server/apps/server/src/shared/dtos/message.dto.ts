export class MessageDTO {
  public sender: string;

  public receiver: string;

  public room: string;

  public message: string;
}
