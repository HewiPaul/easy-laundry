import { ApiProperty } from '@nestjs/swagger';
import * as bcrypt from 'bcrypt';
import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class LoginDTO {
  @ApiProperty()
  @IsEmail()
  @IsString()
  @IsNotEmpty()
  email: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  password: string;

  async comparePassword(attempt: string) {
    return await bcrypt.compare(attempt, this.password);
  }
}
