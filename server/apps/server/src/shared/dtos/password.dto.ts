import { BeforeUpdate } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { Exclude } from 'class-transformer';

export class ForgotPasswordDTO {
  @Exclude()
  password: string;

  @BeforeUpdate()
  async hashPassword() {
    this.password = await bcrypt.hash(this.password, 10);
  }
}

export class ChangePasswordDTO {
  @Exclude()
  oldPassword: string;

  @Exclude()
  newPassword: string;

  @BeforeUpdate()
  async hashPassword() {
    this.newPassword = await bcrypt.hash(this.newPassword, 10);
  }
}
